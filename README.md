# Server init

```bash
$ docker-compose run --rm influxdb bash
$ influx -database 'telegraf'
> USE telegraf
> SHOW SERIES
> SHOW MEASUREMENTS
> SELECT * FROM cpu WHERE time > now()-5m
```
